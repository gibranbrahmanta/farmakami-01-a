from django.urls import path
from .views import *

app_name = 'transaksi_resep'
urlpatterns = [
	path('', transaksi_dengan_resep, name='transaksi_dengan_resep'),
	path('create_transaksi_resep', create_transaksi_resep, name='create_transaksi_resep'),
	path('update_transaksi_resep/<str:no_resep>', update_transaksi_resep, name='update_transaksi_resep'),
	path('delete_transaksi_resep/<str:no_resep>', delete_transaksi_resep, name='delete_transaksi_resep')
]