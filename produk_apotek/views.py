from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *


# Create your views here.
def rud_produk_apotek(request):
    with connection.cursor() as c:
        c.execute("SELECT * FROM PRODUK_APOTEK")
        res = dictfetchall(c)
    response = {'data_produk_apotek':res}
    return render(request,'rud_produk_apotek.html',response)

def update_produk_apotek(request, id_apotek, id_produk):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('auth:login'))
    if request.session['role'] != 'admin_apotek':
        return HttpResponseRedirect(reverse('produk_apotek:rud_produk_apotek'))
    with connection.cursor() as c:
        c.execute("SELECT * FROM PRODUK_APOTEK WHERE id_apotek = %s and id_produk = %s",[id_apotek,id_produk])
        res = dictfetchall(c)
    initial_data = {
        'id_produk' : res[0]['id_produk'],
        'id_apotek' : res[0]['id_apotek'],
        'harga_jual' : res[0]['harga_jual'],
        'satuan_penjualan' : res[0]['satuan_penjualan'],
        'stok' : res[0]['stok']
    }
    if request.method == 'POST':
        form = updateForm(request.POST)
        if form.is_valid() and form.has_changed():
            id_produk = initial_data['id_produk']
            id_apotek = initial_data['id_apotek']
            for field in form.changed_data:
                with connection.cursor() as c:
                    c.execute("UPDATE PRODUK_APOTEK SET %s = %s WHERE id_produk = %s and id_apotek = %s" 
                    % (field, '%s', '%s', '%s'),[form.cleaned_data[field], id_produk, id_apotek]
                    )
                if field == 'id_produk' or field == 'id_apotek':
                    if field == 'id_apotek':
                        if form.cleaned_data['id_apotek'] != id_apotek:
                            id_apotek = form.cleaned_data['id_apotek']
                    else:
                        if form.cleaned_data['id_produk'] != id_produk:
                            id_produk = form.cleaned_data['id_produk']
        return HttpResponseRedirect(reverse('produk_apotek:rud_produk_apotek'))
    update_form = updateForm(initial=initial_data)
    response = {'update_form':update_form}
    return render(request,'update_produk_apotek.html',response)

def create_produk_apotek(request):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('auth:login'))
    if request.session['role'] != 'admin_apotek':
        return HttpResponseRedirect(reverse('produk_apotek:rud_produk_apotek'))
    if request.method == 'POST':
        form = createForm(request.POST)
        if form.is_valid():
            id_produk = form.cleaned_data['id_produk']
            id_apotek = form.cleaned_data['id_apotek']
            harga_jual = form.cleaned_data['harga_jual']
            satuan_penjualan = form.cleaned_data['satuan_penjualan']
            stok = form.cleaned_data['stok']
            with connection.cursor() as c:
                c.execute("INSERT INTO PRODUK_APOTEK VALUES ( %s , %s , %s , %s, %s)",
                            [harga_jual,stok,satuan_penjualan,id_produk,id_apotek]
                )
        return HttpResponseRedirect(reverse('produk_apotek:rud_produk_apotek'))
    create_form = createForm()
    response = {'create_form':create_form}
    return render(request,'create_produk_apotek.html',response)

def delete_produk_apotek(request, id_apotek, id_produk):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('auth:login'))
    if request.session['role'] != 'admin_apotek':
        return HttpResponseRedirect(reverse('produk_apotek:rud_produk_apotek'))
    with connection.cursor() as c:
        c.execute("DELETE FROM PRODUK_APOTEK WHERE id_apotek = %s and id_produk = %s",[id_apotek,id_produk])
    return HttpResponseRedirect(reverse('produk_apotek:rud_produk_apotek'))

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]