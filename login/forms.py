from django import forms

class login_form(forms.Form):
    email = forms.EmailField(widget=forms.widgets.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))


class registerAdminForm(forms.Form):
    
    email = forms.EmailField(widget=forms.widgets.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon ',
        'type' : 'text',
        'required': False,
    }))

class registerKonsumenForm(forms.Form):

    email = forms.EmailField(widget=forms.widgets.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon',
        'type' : 'text',
        'required': False,
    }))

    jenis_kelamin = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jenis Kelamin',
        'type' : 'text',
        'required': True,
    }))

    tanggal_lahir = forms.DateField(input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
    'class': 'form-control',
    'placeholder': 'Tanggal Lahir',
    'type': 'date',
    'required': True,
    }))

class registerKurirForm(forms.Form):

    email = forms.EmailField(widget=forms.widgets.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon',
        'type' : 'text',
        'required': False,
    }))

    nama_perusahaan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Perusahaan',
        'type' : 'text',
        'required': True,
    }))

class registerCSForm(forms.Form):

    email = forms.EmailField(widget=forms.widgets.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password",
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon',
        'type' : 'text',
        'required': False,
    }))

    no_KTP = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor KTP',
        'type' : 'text',
        'required': True,
    }))

    no_SIA = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor SIA',
        'type' : 'text',
        'required': True,
    }))





