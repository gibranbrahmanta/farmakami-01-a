from django.urls import path
from .views import *

app_name = 'obat'
urlpatterns = [
	path('', rud_obat, name = 'rud_obat'),
	path('create_obat', create_obat, name = 'create_obat'),
	path('update_obat/<str:key>', update_obat, name = 'update_obat'),
	path('delete_obat/<str:key>', delete_obat, name = 'delete_obat')
]