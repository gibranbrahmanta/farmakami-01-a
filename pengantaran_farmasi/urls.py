from django.urls import path
from .views import *

app_name = 'pengantaran_farmasi'
urlpatterns = [
	path('', rud_pengantaran_farmasi, name='rud_pengantaran_farmasi'),
	path('update_pengantaran_farmasi/<str:id_pengantaran>',update_pengantaran_farmasi, name='update_pengantaran_farmasi'),
	path('create_pengantaran_farmasi',create_pengantaran_farmasi, name='create_pengantaran_farmasi'),
	path('delete_pengantaran_farmasi/<str:id_pengantaran>',delete_pengantaran_farmasi, name='delete_pengantaran_farmasi')
]