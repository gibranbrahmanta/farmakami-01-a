from django.urls import path
from .views import *

app_name = 'alat_medis'
urlpatterns = [
	path('', alat_medis, name='alat_medis'),
	path('create_alat_medis', create_alat_medis, name='create_alat_medis'),
	path('update_alat_medis/<str:id_alat_medis>/<str:id_produk>', update_alat_medis, name='update_alat_medis'),
	path('delete_alat_medis/<str:id_alat_medis>/<str:id_produk>', delete_alat_medis, name='delete_alat_medis')
]