from django import forms
from django.db import connection

search_path = "SET search_path to FARMAKAMI"

def get_id_apotek_list():
    with connection.cursor() as c:
        c.execute(search_path)
        c.execute("SELECT id_apotek FROM APOTEK")
        res = dictfetchall(c)
    temp_res = [res[i]['id_apotek'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]

def get_id_produk_list():
    with connection.cursor() as c:
        c.execute(search_path)
        c.execute("SELECT id_produk FROM PRODUK")
        res = dictfetchall(c)
    temp_res = [res[i]['id_produk'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

class updateForm(forms.Form):

    id_produk = forms.CharField(label="ID Produk", widget=forms.Select(choices=get_id_produk_list()))

    id_apotek = forms.CharField(label="ID Apotek", widget=forms.Select(choices=get_id_apotek_list()))

    harga_jual = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Harga Jual',
        'type' : 'integer',
        'required': True,
    }))

    satuan_penjualan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Satuan Penjualan',
        'type' : 'text',
        'required': True,
    }))

    stok = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Stok',
        'type' : 'text',
        'required': True,
    }))

class createForm(forms.Form):
    
    id_produk = forms.CharField(label="ID Produk", widget=forms.Select(choices=get_id_produk_list()))

    id_apotek = forms.CharField(label="ID Apotek", widget=forms.Select(choices=get_id_apotek_list()))

    harga_jual = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Harga Jual',
        'type' : 'integer',
        'required': True,
    }))

    satuan_penjualan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Satuan Penjualan',
        'type' : 'text',
        'required': True,
    }))

    stok = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Stok',
        'type' : 'integer',
        'required': True,
    }))

