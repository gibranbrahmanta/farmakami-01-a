from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def profil(request):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('auth:login'))
    email = request.session['email']
    role= request.session['role']
    cursor_p = connection.cursor()

    select = "SELECT * FROM FARMAKAMI.PENGGUNA WHERE email='"+email+"';"
    cursor_p.execute(select)
    data = namedtuplefetchall(cursor_p)
    general_data = data[0]

    if role =='admin_apotek':
        select="SELECT * FROM FARMAKAMI.ADMIN_APOTEK WHERE email='"+email+"';"
        cursor_p.execute(select)
        data  = namedtuplefetchall(cursor_p)
        data_admin = data[0]
        return render(request,'profil.html',{'general_data':general_data,'data_admin':data_admin})

    elif role == 'konsumen':
        select="SELECT * FROM FARMAKAMI.KONSUMEN WHERE email='"+email+"';"
        cursor_p.execute(select)
        data= namedtuplefetchall(cursor_p)
        data_konsumen = data[0]

        id_konsumen = data_konsumen.id_konsumen
        select = "SELECT * FROM FARMAKAMI.ALAMAT_KONSUMEN WHERE id_konsumen='"+id_konsumen+"';"
        cursor_p.execute(select)
        data = namedtuplefetchall(cursor_p)
        alamat_konsumen= data

        return render(request,'profilKonsumen.html',{'general_data':general_data,
        'data_konsumen':data_konsumen,'alamat_konsumen':alamat_konsumen})

    elif role == 'cs':
        select="SELECT * FROM FARMAKAMI.CS WHERE email='"+email+"';"
        cursor_p.execute(select)
        data  = namedtuplefetchall(cursor_p)
        data_cs = data[0]
        return render(request,'profil_CS.html',{'general_data':general_data,'data_cs':data_cs})

    elif role == 'kurir':
        select="SELECT * FROM FARMAKAMI.KURIR WHERE email='"+email+"';"
        cursor_p.execute(select)
        data  = namedtuplefetchall(cursor_p)
        data_kurir = data[0]
        return render(request,'profil_kurir.html',{'general_data':general_data,'data_kurir':data_kurir})
    

        




