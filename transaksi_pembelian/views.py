from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
import datetime

def CreateTransaksiPmbl(request):
    cursor_tp= connection.cursor()
    email = request.session['email']
    role = request.session['role']
    if request.method =="POST":
        id_konsumen_views = request.POST['id_konsumen']
    
        select = 'SELECT COUNT(*) FROM FARMAKAMI.TRANSAKSI_PEMBELIAN'
        cursor_tp.execute(select)
        generate_id_tp= cursor_tp.fetchone()[0]
        generate_id_tp = int(generate_id_tp)+1
        generate_tp = "TP0" + str(generate_id_tp)

        select = "SELECT COUNT(*) FROM FARMAKAMI.TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian='" + generate_tp + "'"
        cursor_tp.execute(select)
        test = cursor_tp.fetchone()[0]
        while test != 0:
            generate_id_tp = generate_id_tp+1
            generate_tp = "TP0" + str(generate_id_tp)
            select = "SELECT COUNT(*) FROM FARMAKAMI.TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian='" + generate_tp + "'"
            cursor_tp.execute(select)
            test=cursor_tp.fetchone()[0]
        select = "INSERT INTO FARMAKAMI.TRANSAKSI_PEMBELIAN VALUES ( %s , %s , 0 , %s);"
        cursor_tp.execute(select,(generate_tp,datetime.datetime.now(),id_konsumen_views))
        return redirect ('/transaksi_pembelian/')
    ambil ='SELECT id_konsumen FROM FARMAKAMI.konsumen'
    cursor_tp.execute(ambil)
    data = namedtuplefetchall(cursor_tp)
    return render(request,'create_transaksipmbl.html',{'data':data})
    
def DaftarTransaksi(request):
    cursor_dt = connection.cursor()
    email = request.session['email']
    role = request.session['role']
    data_set=[]
    if role == 'konsumen':
        select = "SELECT * FROM FARMAKAMI.konsumen WHERE email ='"+ email +"';"
        cursor_dt.execute(select)
        data_set= namedtuplefetchall(cursor_dt)
        id_konsumen=data_set[0].id_konsumen
        select_dt = "SELECT * FROM FARMAKAMI.TRANSAKSI_PEMBELIAN WHERE id_konsumen ='"+id_konsumen+"';"
        cursor_dt.execute(select_dt)
        data_set = namedtuplefetchall(cursor_dt)
        return render(request, 'read_TPKonsumen.html', {'data_set':data_set})
    else:
        select = "SELECT * FROM FARMAKAMI.TRANSAKSI_PEMBELIAN ORDER BY id_transaksi_pembelian ASC;"
        cursor_dt.execute(select)
        data_tp= namedtuplefetchall(cursor_dt)
        return render(request,'rud_transaksipmbl.html',{'data_tp':data_tp})

def updateTP(request,id):
    cursor_tp = connection.cursor()

    if request.method =='POST':
        id_konsumen_views = request.POST['id_konsumen']
        waktu= datetime.datetime.now()
        waktu_pembelian = str(waktu)

        if id_konsumen_views == "none": 
            search_id = "SELECT id_konsumen FROM FARMAKAMI.TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian ='" + id + "'" + ";"
            cursor_tp.execute(search_id)
            id_konsumen_views= cursor_tp.fetchone()[0]
        
        data_tp = "UPDATE FARMAKAMI.TRANSAKSI_PEMBELIAN SET id_konsumen ='" + id_konsumen_views + "' WHERE id_transaksi_pembelian='"+id+"';"
        cursor_tp.execute(data_tp)
        data_tp = "UPDATE FARMAKAMI.TRANSAKSI_PEMBELIAN SET waktu_pembelian ='" + waktu_pembelian + "' WHERE id_transaksi_pembelian='"+id+"';"
        cursor_tp.execute(data_tp)
        return redirect('/transaksi_pembelian/')
        
    ambil ='SELECT id_konsumen FROM FARMAKAMI.konsumen'
    cursor_tp.execute(ambil)
    id_konsumen = namedtuplefetchall(cursor_tp)

    select="SELECT waktu_pembelian FROM FARMAKAMI.TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian='"+id+"';"
    cursor_tp.execute(select)
    waktu =cursor_tp.fetchone()[0]


    select2= "SELECT total_pembayaran FROM FARMAKAMI.TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian='"+id+"';"
    cursor_tp.execute(select2)
    total_pembayaran = cursor_tp.fetchone()[0]

    
    return render(request,'update_transaksipmbl.html',{'id':id,
    'waktu':waktu,
    'total_pembayaran':total_pembayaran,
    'id_konsumen':id_konsumen
    })

def DeleteTransaksi(request,id):
    cursor_dt = connection.cursor()
    delete_dt = "DELETE FROM FARMAKAMI.TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian='" + id+ "';"
    cursor_dt.execute(delete_dt)
    return redirect('/transaksi_pembelian/')

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]