"""farmakami URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

import homepage.urls
import apotek.urls
import produk_apotek.urls
import login.urls
import alat_medis.urls
import balai_pengobatan.urls
import list_produk.urls
import obat.urls
import pengantaran_farmasi.urls
import profil.urls
import transaksi_pembelian.urls
import transaksi_resep.urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(homepage.urls)),
    path('apotek/',include(apotek.urls)),
    path('produk_apotek/',include(produk_apotek.urls)),
    path('auth/',include(login.urls)),
    path('alat_medis/',include(alat_medis.urls)),
    path('balai_pengobatan/',include(balai_pengobatan.urls)),
    path('list_produk/',include(list_produk.urls)),
    path('obat/',include(obat.urls)),
    path('pengantaran_farmasi/',include(pengantaran_farmasi.urls)),
    path('profil/',include(profil.urls)),
    path('transaksi_pembelian/',include(transaksi_pembelian.urls)),
    path('transaksi_resep/',include(transaksi_resep.urls))
]

