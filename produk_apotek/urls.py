from django.urls import path
from .views import *

app_name = 'produk_apotek'
urlpatterns = [
	path('', rud_produk_apotek, name='rud_produk_apotek'),
	path('update_produk_apotek/<str:id_produk>/<str:id_apotek>',update_produk_apotek, name='update_produk_apotek'),
	path('delete_produk_apotek/<str:id_produk>/<str:id_apotek>',delete_produk_apotek, name='delete_produk_apotek'),
	path('create_produk_apotek',create_produk_apotek, name='create_produk_apotek')
]