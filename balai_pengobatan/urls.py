from django.urls import path
from .views import *

app_name = 'balai_pengobatan'
urlpatterns = [
	path('', rud_balai_pengobatan, name = 'rud_balai_pengobatan'),
	path('create_balai_pengobatan', create_balai_pengobatan, name = 'create_balai_pengobatan'),
	path('update_balai_pengobatan/<str:key>', update_balai_pengobatan, name = 'update_balai_pengobatan'),
	path('delete_balai_pengobatan/<str:key>', delete_balai_pengobatan, name = 'delete_balai_pengobatan')
]