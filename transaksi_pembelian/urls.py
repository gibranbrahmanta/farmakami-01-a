from django.urls import path
from .views import *

app_name = 'TransaksiPembelian'
urlpatterns = [
	path('create_TransaksiPmbl', CreateTransaksiPmbl, name='CreateTransaksiPmbl'),
	path('', DaftarTransaksi, name='DaftarTransaksi'),
	path('update_TP/<id>',updateTP,name='updateTP' ),
	path('deleteTP/<id>',DeleteTransaksi,name='DeleteTransaksi')
]