from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

def index(request):
    if 'email' in request.session:
        return HttpResponseRedirect(reverse('profil:profil'))
    response = {}
    return render(request,'homepage.html',response)

def register(request):
    response = {}
    return render(request,'register.html',response)