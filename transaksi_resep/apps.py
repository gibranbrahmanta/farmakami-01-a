from django.apps import AppConfig


class TransaksiResepConfig(AppConfig):
    name = 'transaksi_resep'
