from django.urls import path
from .views import *

app_name = 'list_produk'
urlpatterns = [
	path('', rud_list_produk, name='rud_list_produk'),
	path('create_list_produk',create_list_produk, name='create_list_produk'),
	path('update_list_produk/<str:id_produk>/<str:id_apotek>/<str:id_transaksi_pembelian>',update_list_produk, name='update_list_produk'),
	path('delete_list_produk/<str:id_produk>/<str:id_apotek>/<str:id_transaksi_pembelian>',delete_list_produk, name='delete_list_produk')
]