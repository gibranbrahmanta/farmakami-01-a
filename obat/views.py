from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *

# Create your views here.
def rud_obat(request):
	with connection.cursor() as c:
		c.execute("SELECT * FROM OBAT ORDER BY id_obat ASC")
		res = dictfetchall(c)
	response = {'data_obat': res}
	return render(request,'rud_obat.html', response)

def create_obat(request):
	if  request.method == 'POST':
		form = createForm(request.POST)
		if form.is_valid():
			with connection.cursor() as c:
				c.execute("SELECT * from OBAT ORDER BY id_obat desc limit 1")
				last_data_obat = dictfetchall(c)
			last_id_obat = last_data_obat[0]['id_obat']
			updated_id_obat = int(last_id_obat[3:]) + 1
			updated_id_obat_str = '{:03d}'.format(updated_id_obat)
			id_obat = 'OBT' + updated_id_obat_str
			with connection.cursor() as ci:
				ci.execute("SELECT * from OBAT ORDER BY id_produk desc limit 1")
				last_data_produk = dictfetchall(ci)
			last_id_produk = last_data_produk[0]['id_produk']
			updated_id_produk = int(last_id_produk[3:]) + 1
			updated_id_produk_str = '{:02d}'.format(updated_id_produk)
			id_produk = 'KST' + updated_id_produk_str
			id_merk_obat = form.cleaned_data['id_merk_obat']
			netto = form.cleaned_data['netto']
			dosis = form.cleaned_data['dosis']
			aturan_pakai = form.cleaned_data['aturan_pakai']
			kontraindaksi = form.cleaned_data['kontraindaksi']
			bentuk_kesediaan = form.cleaned_data['bentuk_kesediaan']
			with connection.cursor() as ca:
				ca.execute("INSERT INTO OBAT VALUES ( %s, %s, %s, %s, %s, %s, %s, %s)",
					[id_obat, id_produk, id_merk_obat, netto, dosis, aturan_pakai, kontraindaksi, bentuk_kesediaan])
		return HttpResponseRedirect(reverse('obat:rud_obat'))
	create_form_obat = createForm()
	response = {'create_form' : create_form_obat}
	return render(request, 'create_obat.html', response)
	

def update_obat(request, key):
	if request.method == 'POST':
		form = updateForm(request.POST)
		if form.is_valid():
			id_obat = form.cleaned_data['id_obat']
			id_produk = form.cleaned_data['id_produk']
			id_merk_obat = form.cleaned_data['id_merk_obat']
			netto = form.cleaned_data['netto']
			dosis = form.cleaned_data['dosis']			
			aturan_pakai = form.cleaned_data['aturan_pakai']
			kontraindaksi = form.cleaned_data['kontraindaksi']
			bentuk_kesediaan = form.cleaned_data['bentuk_kesediaan']
			with connection.cursor() as c:
				c.execute("""INSERT INTO OBAT VALUES ( %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (id_obat)
					DO UPDATE SET id_merk_obat = %s, netto = %s, dosis = %s, aturan_pakai = %s, kontraindaksi = %s, bentuk_kesediaan = %s""", 
					[id_obat, id_produk, id_merk_obat, netto, dosis, aturan_pakai, kontraindaksi, bentuk_kesediaan, id_merk_obat, netto, dosis, aturan_pakai, kontraindaksi, bentuk_kesediaan])
		return HttpResponseRedirect(reverse('obat:rud_obat'))
	with connection.cursor() as c:
		c.execute("SELECT * FROM OBAT WHERE id_obat = %s", [key])
		res = dictfetchall(c)
	initial_data = {
		'id_obat' : res[0]['id_obat'],
		'id_produk' : res[0]['id_produk'],
		'id_merk_obat' : res[0]['id_merk_obat'],
		'netto' : res[0]['netto'],
		'dosis' : res[0]['dosis'],
		'aturan_pakai' : res[0]['aturan_pakai'],
		'kontraindaksi' : res[0]['kontraindaksi'],
		'bentuk_kesediaan' : res[0]['bentuk_kesediaan'],
	}
	update_form = updateForm(initial = initial_data)
	response = {'update_form' : update_form}
	return render(request,'update_obat.html', response)

def delete_obat(request, key):
	with connection.cursor() as c:
		c.execute("DELETE FROM OBAT WHERE id_obat = %s", [key])
		return HttpResponseRedirect(reverse('obat:rud_obat'))

def dictfetchall(cursor):
	"Return all rows from a cursor as a dict"
	columns = [col[0] for col in cursor.description]
	return [
	dict(zip(columns, row))
	for row in cursor.fetchall()
	]