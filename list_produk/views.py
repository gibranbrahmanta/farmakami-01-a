from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *

def rud_list_produk(request):
	with connection.cursor() as c:
		c.execute("SELECT * FROM LIST_PRODUK_DIBELI")
		res = dictfetchall(c)
	response = {'data_list_produk':res}
	return render(request,'rud_list_produk.html',response)

def create_list_produk(request):
    if request.method == 'POST':
        form = createForm(request.POST)
        if form.is_valid():
            id_produk = form.cleaned_data['id_produk']
            id_apotek = form.cleaned_data['id_apotek']
            id_transaksi_pembelian = form.cleaned_data['id_transaksi_pembelian']
            jumlah = form.cleaned_data['jumlah']
            with connection.cursor() as c:
                c.execute("INSERT INTO LIST_PRODUK_DIBELI VALUES (%s, %s, %s, %s)",[jumlah, id_apotek, id_produk, id_transaksi_pembelian])
        return HttpResponseRedirect(reverse('list_produk:rud_list_produk'))
    create_form_produk = createForm()
    response = {'create_form_produk':create_form_produk}
    return render(request,'create_list_produk.html',response)


def update_list_produk(request, id_apotek, id_produk, id_transaksi_pembelian):
    with connection.cursor() as c:
        c.execute("SELECT * FROM LIST_PRODUK_DIBELI WHERE id_produk = %s and id_apotek = %s",[id_produk,id_apotek])
        res = dictfetchall(c)
    initial_data = {
        'id_produk' : res[0]['id_produk'],
        'id_apotek' : res[0]['id_apotek'],
        'id_transaksi_pembelian' : res[0]['id_transaksi_pembelian'],
        'jumlah' : res[0]['jumlah']
    }
    if request.method == 'POST':
        form = updateForm(request.POST)
        if form.is_valid() and form.has_changed():
            id_produk = initial_data['id_produk']
            id_apotek = initial_data['id_apotek']
            id_transaksi_pembelian = initial_data['id_transaksi_pembelian']
            for field in form.changed_data:
                with connection.cursor() as c:
                     c.execute("UPDATE LIST_PRODUK_DIBELI SET %s = %s WHERE id_transaksi_pembelian = %s and id_apotek = %s and id_produk = %s" 
                    % (field, '%s', '%s', '%s','%s'),[form.cleaned_data[field], id_transaksi_pembelian, id_apotek, id_produk]
                    )
                if field == 'id_transaksi_pembelian':
                    id_transaksi_pembelian = form.cleaned_data['id_transaksi_pembelian']
        return HttpResponseRedirect(reverse('list_produk:rud_list_produk'))
    update_form_produk = updateForm(initial = initial_data)
    response = {'update_form_produk':update_form_produk}
    return render(request,'update_list_produk.html',response)

def delete_list_produk(request,id_produk, id_apotek, id_transaksi_pembelian):
    with connection.cursor() as c:
        c.execute("DELETE FROM LIST_PRODUK_DIBELI WHERE id_produk = %s and id_apotek = %s and id_transaksi_pembelian = %s",[id_produk,id_apotek,id_transaksi_pembelian])
    return HttpResponseRedirect(reverse('list_produk:rud_list_produk'))

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


