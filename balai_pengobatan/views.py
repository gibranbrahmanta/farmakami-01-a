from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from collections import namedtuple
from .forms import *

# Create your views here.
def rud_balai_pengobatan(request):
	with connection.cursor() as c:
		c.execute("SELECT bp.id_balai,alamat_balai,nama_balai,jenis_balai, telepon_balai, id_apotek FROM BALAI_PENGOBATAN as bp JOIN BALAI_APOTEK as ba ON bp.id_balai = ba.id_balai")
		res1 = dictfetchall(c)
	response = {'data_balai_pengobatan':res1}
	return render(request,'rud_balai_pengobatan.html', response)

def create_balai_pengobatan(request):
	if request.method == 'POST':
		form = createForm(request.POST)
		if form.is_valid():
			with connection.cursor() as ci:
				ci.execute("SELECT * FROM BALAI_PENGOBATAN ORDER BY id_balai desc limit 1")
				last_data = dictfetchall(ci)
			last_id = last_data[0]['id_balai']
			updated_id = int(last_id[2:]) + 1
			updated_id_str = '{:03d}'.format(updated_id)
			id_balai = 'BL' + updated_id_str
			alamat_balai = form.cleaned_data['alamat_balai']
			nama_balai = form.cleaned_data['nama_balai']
			jenis_balai = form.cleaned_data['jenis_balai']
			telepon_balai = form.cleaned_data['telepon_balai']
			id_apotek = form.cleaned_data['id_apotek']
			with connection.cursor() as c:
				c.execute("INSERT INTO BALAI_PENGOBATAN VALUES ( %s, %s, %s, %s, %s)", 
					[id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai])
				c.execute("INSERT INTO BALAI_APOTEK VALUES (%s, %s)",
					[id_balai, id_apotek])
		return HttpResponseRedirect(reverse('balai_pengobatan:rud_balai_pengobatan'))
	create_form = createForm()
	response = {'create_form' : create_form}
	return render(request, 'create_balai_pengobatan.html', response)

def update_balai_pengobatan(request, key):
	if request.method == 'POST':
		form = updateForm(request.POST)
		if form.is_valid():
			id_balai = form.cleaned_data['id_balai']
			alamat_balai = form.cleaned_data['alamat_balai']
			nama_balai = form.cleaned_data['nama_balai']
			jenis_balai = form.cleaned_data['jenis_balai']
			telepon_balai = form.cleaned_data['telepon_balai']
			id_apotek = form.cleaned_data['id_apotek']
			with connection.cursor() as c:
				c.execute("""INSERT INTO BALAI_PENGOBATAN VALUES ( %s, %s, %s, %s, %s) ON CONFLICT (id_balai)
					DO UPDATE SET alamat_balai = %s, nama_balai = %s, jenis_balai = %s, telepon_balai = %s""", 
					[id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai])
				c.execute("UPDATE BALAI_APOTEK SET id_apotek = %s WHERE id_balai = %s", [id_apotek, key])
		return HttpResponseRedirect(reverse('balai_pengobatan:rud_balai_pengobatan'))	
	with connection.cursor() as c:
		c.execute("SELECT bp.id_balai,alamat_balai,nama_balai,jenis_balai, telepon_balai, id_apotek FROM BALAI_PENGOBATAN as bp JOIN BALAI_APOTEK as ba ON bp.id_balai = ba.id_balai WHERE bp.id_balai = %s", [key])
		res = dictfetchall(c)
	initial_data = {
		'id_balai' : res[0]['id_balai'],
		'alamat_balai' : res[0]['alamat_balai'],
		'nama_balai' : res[0]['nama_balai'],
		'jenis_balai' : res[0]['jenis_balai'],
		'telepon_balai' : res[0]['telepon_balai'],
		'id_apotek' : res[0]['id_apotek'],
	}
	update_form = updateForm(initial = initial_data)
	response = {'update_form' : update_form}
	return render(request,'update_balai_pengobatan.html', response)

def delete_balai_pengobatan(request, key):
	with connection.cursor() as c:
		c.execute("DELETE FROM BALAI_PENGOBATAN WHERE id_balai = %s", [key])
		c.execute("DELETE FROM BALAI_APOTEK WHERE id_balai = %s", [key])
		return HttpResponseRedirect(reverse('balai_pengobatan:rud_balai_pengobatan'))

def dictfetchall(cursor):
	"Return all rows from a cursor as a dict"
	columns = [col[0] for col in cursor.description]
	return [
		dict(zip(columns, row))
		for row in cursor.fetchall()
	]

