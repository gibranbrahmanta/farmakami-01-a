# Farma Kami

**Kelas**: A
**Kelompok**: 01    

**Anggota** :  
  - Astrid Chaerida : 1806191370
  - Gibran Brahmanta P. : 1806186553
  - Muhammad Marandi Millendila Efendi : 1806191490 
  - Natasya Elora Carissa : 1806147086
  - Novia Ramadani : 1806147092

**Link Website**  
[https://farmakami-01-a.herokuapp.com/](https://farmakami-01-a.herokuapp.com/)