from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *

# Create your views here.
def alat_medis(request):
    with connection.cursor() as c:
        c.execute("SELECT * FROM ALAT_MEDIS ORDER BY id_alat_medis ASC")
        res = dictfetchall(c)
    response = {'data_alat_medis':res}
    return render(request,'alat_medis.html',response)

def create_alat_medis(request):
    if request.method == 'POST' :
        form = createForm(request.POST)
        if form.is_valid():
            with connection.cursor() as c:
               c.execute("SELECT * FROM ALAT_MEDIS ORDER BY id_alat_medis desc limit 1")
               last_data = dictfetchall(c)
            last_id_am = last_data[0]['id_alat_medis']
            updated_last_am = int(last_id_am[2:])+1
            updated_last_am_str = '{:03d}'.format(updated_last_am)
            id_alat_medis = 'AM' + updated_last_am_str
            nama_alat_medis = form.cleaned_data['nama_alat_medis']
            deskripsi_alat = form.cleaned_data['deskripsi_alat']
            jenis_penggunaan = form.cleaned_data['jenis_penggunaan']
            with connection.cursor() as ci:
                ci.execute("SELECT * FROM PRODUK ORDER BY id_produk desc limit 1")
                last_data_produk = dictfetchall(ci)
            last_id_produk = last_data_produk[0]['id_produk']
            updated_last_produk = int(last_id_produk[3:])+1
            id_produk = 'KST' + str(updated_last_produk)
            with connection.cursor() as c:
                c.execute("INSERT INTO PRODUK VALUES ( %s )",[id_produk])
                c.execute("INSERT INTO ALAT_MEDIS VALUES( %s , %s , %s , %s , %s)",
                [id_alat_medis, nama_alat_medis, deskripsi_alat, jenis_penggunaan, id_produk])
        return HttpResponseRedirect(reverse('alat_medis:alat_medis'))
    create_form = createForm()
    response = {'create_form':create_form}
    return render(request,'create_alat_medis.html',response)

def update_alat_medis(request, id_alat_medis, id_produk):
    if request.method == 'POST':
        form = updateForm(request.POST)
        if form.is_valid():
            id_alat_medis = form.cleaned_data['id_alat_medis']
            nama_alat_medis = form.cleaned_data['nama_alat_medis']
            deskripsi_alat = form.cleaned_data['deskripsi_alat']
            jenis_penggunaan = form.cleaned_data['jenis_penggunaan']
            id_produk = form.cleaned_data['id_produk']
            with connection.cursor() as c:
                c.execute("""INSERT INTO ALAT_MEDIS VALUES ( %s , %s , %s , %s , %s) ON CONFLICT (id_alat_medis)
                    DO UPDATE SET nama_alat_medis = %s, deskripsi_alat = %s, jenis_penggunaan = %s""",
                    [id_alat_medis, nama_alat_medis, deskripsi_alat, jenis_penggunaan, id_produk,
                    nama_alat_medis, deskripsi_alat, jenis_penggunaan])
        return HttpResponseRedirect(reverse('alat_medis:alat_medis'))
    with connection.cursor() as c:
        c.execute("SELECT * FROM ALAT_MEDIS WHERE id_alat_medis = %s and id_produk = %s", [id_alat_medis, id_produk])
        res = dictfetchall(c)
    initial_data = {
        'id_alat_medis' : res[0]['id_alat_medis'],
        'nama_alat_medis' :res[0]['nama_alat_medis'],
        'deskripsi_alat' : res[0]['deskripsi_alat'],
        'jenis_penggunaan' : res[0]['jenis_penggunaan'],
        'id_produk' : res[0]['id_produk']
    }
    update_form = updateForm(initial=initial_data)
    response = {'update_form':update_form}
    return render(request,'update_alat_medis.html',response)

def delete_alat_medis(request, id_alat_medis, id_produk):
    with connection.cursor() as c:
        c.execute("DELETE FROM ALAT_MEDIS WHERE id_alat_medis = %s AND id_produk = %s",
            [id_alat_medis, id_produk])
        c.execute("DELETE FROM PRODUK WHERE id_produk = %s",['id_produk'])
    return HttpResponseRedirect(reverse('alat_medis:alat_medis'))

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]