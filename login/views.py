from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import *

# Create your views here.
def register_admin(request):
    if request.method == 'POST':
        registform = registerAdminForm(request.POST)
        if registform.is_valid():
            email = registform.cleaned_data['email']
            password = registform.cleaned_data['password']
            nama = registform.cleaned_data['nama_lengkap']
            no_telp = registform.cleaned_data['no_telepon']
            if check_user_is_exist("PENGGUNA",email) == False:
                register(email,password,nama,no_telp)
                with connection.cursor() as c:
                    c.execute("INSERT INTO APOTEKER VALUES (%s)",[email])
                    c.execute("INSERT INTO ADMIN_APOTEK VALUES (%s, %s)",[email,None])
                request.session['email'] = email
                request.session['password'] = password
                request.session['role'] = 'admin_apotek'
                return HttpResponseRedirect(reverse('profil:profil'))
            else:
                messages.error(request, 'Email sudah terdaftar')
                return HttpResponseRedirect(reverse('auth:register_admin'))
    register_form = registerAdminForm()
    response = {'register_form':register_form}
    return render(request,'register_admin.html',response)

def register_konsumen(request):
    if request.method == 'POST':
        registform = registerKonsumenForm(request.POST)
        if registform.is_valid():
            email = registform.cleaned_data['email']
            password = registform.cleaned_data['password']
            nama = registform.cleaned_data['nama_lengkap']
            no_telp = registform.cleaned_data['no_telepon']
            jenis_kelamin = registform.cleaned_data['jenis_kelamin']
            tanggal_lahir = registform.cleaned_data['tanggal_lahir']
            lst_alamat = []
            index = 1
            while True:
                try:
                    tmp_status = request.POST['status-alamat-'+str(index)]
                    tmp_alamat = request.POST['alamat-'+str(index)]
                    if tmp_status != "" and tmp_alamat != "":
                        for alamat in lst_alamat:
                            if alamat[0] == tmp_status and alamat[1] == tmp_alamat:
                                messages.error(request, 'Pasangan status alamat dan alamat tidak boleh ada yang sama')
                                return HttpResponseRedirect(reverse('auth:register_konsumen'))
                        lst_alamat.append([tmp_status,tmp_alamat])
                    index+=1
                except:
                    break
            if check_user_is_exist("PENGGUNA",email) == False:
                register(email,password,nama,no_telp)
                id_konsumen = 'KSM00' + str(get_last_id("KONSUMEN"))
                with connection.cursor() as c:
                    c.execute("INSERT INTO KONSUMEN VALUES (%s, %s, %s, %s)",[id_konsumen,email,jenis_kelamin,tanggal_lahir])
                    for alamat in lst_alamat:
                        c.execute("INSERT INTO ALAMAT_KONSUMEN VALUES (%s, %s, %s)",[id_konsumen,alamat[1],alamat[0]])
                request.session['email'] = email
                request.session['password'] = password
                request.session['role'] = 'konsumen'
                return HttpResponseRedirect(reverse('profil:profil'))
            else:
                messages.error(request, 'Email sudah terdaftar')
                return HttpResponseRedirect(reverse('auth:register_konsumen'))
    register_form = registerKonsumenForm()
    response = {'register_form':register_form}
    return render(request,'register_konsumen.html',response)

def register_kurir(request):
    if request.method == 'POST':
        registform = registerKurirForm(request.POST)
        if registform.is_valid():
            email = registform.cleaned_data['email']
            password = registform.cleaned_data['password']
            nama = registform.cleaned_data['nama_lengkap']
            no_telp = registform.cleaned_data['no_telepon']
            nama_perusahaan = registform.cleaned_data['nama_perusahaan']
            if check_user_is_exist("PENGGUNA",email) == False:
                register(email,password,nama,no_telp)
                id_kurir = 'KR00' + str(get_last_id("KURIR"))
                with connection.cursor() as c:
                    c.execute("INSERT INTO KURIR VALUES (%s, %s, %s)",[id_kurir,email,nama_perusahaan])
                request.session['email'] = email
                request.session['password'] = password
                request.session['role'] = 'kurir'
                return HttpResponseRedirect(reverse('profil:profil'))
            else:
                messages.error(request, 'Email sudah terdaftar')
                return HttpResponseRedirect(reverse('auth:register_kurir'))
    register_form = registerKurirForm()
    response = {'register_form':register_form}
    return render(request,'register_kurir.html',response)

def register_CS(request):
    if request.method == 'POST':
        registform = registerCSForm(request.POST)
        if registform.is_valid():
            email = registform.cleaned_data['email']
            password = registform.cleaned_data['password']
            nama = registform.cleaned_data['nama_lengkap']
            no_telp = registform.cleaned_data['no_telepon']
            no_ktp = registform.cleaned_data['no_KTP']
            no_sia = registform.cleaned_data['no_SIA']
            if check_user_is_exist("PENGGUNA",email) == False:
                if check_ktp_cs_is_exist(no_ktp) == False:
                    if check_sia_cs_is_exist(no_sia) == False:
                        register(email,password,nama,no_telp)
                        with connection.cursor() as c:
                            c.execute("INSERT INTO APOTEKER VALUES (%s)",[email])
                            c.execute("INSERT INTO CS VALUES (%s, %s, %s)",[no_ktp,email,no_sia])
                        request.session['email'] = email
                        request.session['password'] = password
                        request.session['role'] = 'cs'
                        return HttpResponseRedirect(reverse('profil:profil'))
                    else:
                        messages.error(request, 'No SIA sudah terdaftar')
                        return HttpResponseRedirect(reverse('auth:register_cs'))
                else:
                    messages.error(request, 'No KTP sudah terdaftar')
                    return HttpResponseRedirect(reverse('auth:register_cs'))
            else:
                messages.error(request, 'Email sudah terdaftar')
                return HttpResponseRedirect(reverse('auth:register_cs'))
    register_form = registerCSForm()
    response = {'register_form':register_form}
    return render(request,'register_cs.html',response)

def login(request):
    if 'email' in request.session:
        return HttpResponseRedirect(reverse('profil:profil'))
    if request.method == 'POST':
        loginform = login_form(request.POST)
        if loginform.is_valid():
            email = loginform.cleaned_data['email']
            password = loginform.cleaned_data['password']
            if check_user_is_exist("PENGGUNA",email,password):
                request.session['email'] = email
                request.session['password'] = password
                role_list = ["ADMIN_APOTEK", "CS", "KONSUMEN", "KURIR"]
                for role in role_list:
                    if check_user_is_exist(role,email):
                        request.session['role'] = role.lower()
                        break
                return HttpResponseRedirect(reverse('profil:profil'))
            else:
                messages.error(request, 'Email/Password salah')
                return HttpResponseRedirect(reverse('auth:login'))
    loginform = login_form()
    response = {'loginform':loginform}
    return render(request,'login.html',response)

def logout(request):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('homepage:index'))
    request.session.flush()
    return HttpResponseRedirect(reverse('homepage:index'))

def check_user_is_exist(table, email, password = None):
    data = {}
    if password is not None :
        with connection.cursor() as c:
            c.execute("SELECT * FROM %s where email = %s and password = %s" % (table, '%s', '%s'),[email,password])
            data = dictfetchall(c)
    else:
        with connection.cursor() as c:
            c.execute("SELECT * FROM %s where email = %s " % (table, '%s'),[email])
            data = dictfetchall(c)
    return len(data) > 0 

def register(email,password,nama,no_telp):
    with connection.cursor() as c:
        c.execute("INSERT INTO PENGGUNA VALUES (%s, %s, %s, %s)",[email,no_telp,password,nama])

def get_last_id(table):
    id_field = ""
    if table == "KURIR":
        id_field = "id_kurir"
    elif table == "KONSUMEN":
        id_field = "id_konsumen"
    with connection.cursor() as c:
        c.execute("SELECT * FROM %s ORDER BY %s desc limit 1" % (table,id_field)) 
        last_data = dictfetchall(c)
        last_id = last_data[0][id_field]
    return int(last_id[-1]) + 1

def check_ktp_cs_is_exist(ktp):
    with connection.cursor() as c:
        c.execute("SELECT * FROM CS where no_ktp = %s",[ktp])
        data = dictfetchall(c)
    return len(data) > 0 

def check_sia_cs_is_exist(sia):
    with connection.cursor() as c:
        c.execute("SELECT * FROM CS where no_sia = %s",[sia])
        data = dictfetchall(c)
    return len(data) > 0 
        
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
