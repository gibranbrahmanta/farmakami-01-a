from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *

# Create your views here.
def transaksi_dengan_resep(request):
	with connection.cursor() as c:
		c.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP ORDER BY no_resep ASC")
		res = dictfetchall(c)
	response = {'data_transaksi_dengan_resep':res}
	return render(request,'transaksi_dengan_resep.html',response)

def create_transaksi_resep(request):
	if request.method == 'POST':
		form = createForm(request.POST)
		if form.is_valid():
			with connection.cursor() as c:
				c.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP ORDER BY no_resep DESC LIMIT 1")
				last_data = dictfetchall(c)
			last_no_resep = last_data[0]['no_resep']
			updated_last_no_resep = int(last_no_resep[3:])+1
			updated_last_no_resep_str = '{:03d}'.format(updated_last_no_resep)
			no_resep = 'RSP' + updated_last_no_resep_str
			nama_dokter = form.cleaned_data['nama_dokter']
			nama_pasien = form.cleaned_data['nama_pasien']
			isi_resep = form.cleaned_data['isi_resep']
			unggahan_resep = form.cleaned_data['unggahan_resep']
			tanggal_resep = form.cleaned_data['tanggal_resep']
			status_validasi = 'Entered'
			no_telepon = form.cleaned_data['no_telepon']
			id_transaksi_pembelian = form.cleaned_data['id_transaksi_pembelian']
			with connection.cursor() as c:
				c.execute("INSERT INTO TRANSAKSI_DENGAN_RESEP VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %s, NULL)",
					[no_resep,nama_dokter,nama_pasien,isi_resep,unggahan_resep,tanggal_resep,status_validasi,no_telepon,id_transaksi_pembelian])
		return HttpResponseRedirect(reverse('transaksi_resep:transaksi_dengan_resep'))
	create_form_resep = createForm()
	response = {'create_form_resep':create_form_resep}
	return render(request,'create_transaksi_resep.html',response)

def update_transaksi_resep(request, no_resep):
	if request.method == 'POST':
		form = updateForm(request.POST)
		if form.is_valid():
			no_resep = form.cleaned_data['no_resep']
			nama_dokter = form.cleaned_data['nama_dokter']
			nama_pasien = form.cleaned_data['nama_pasien']
			isi_resep = form.cleaned_data['isi_resep']
			unggahan_resep = form.cleaned_data['unggahan_resep']
			tanggal_resep = form.cleaned_data['tanggal_resep']
			status_validasi = form.cleaned_data['status_validasi']
			no_telepon = form.cleaned_data['no_telepon']
			id_transaksi_pembelian = form.cleaned_data['id_transaksi_pembelian']
			email_apoteker = form.cleaned_data['email_apoteker']
			try:
				with connection.cursor() as c:
					c.execute("UPDATE transaksi_dengan_resep SET nama_dokter=%s, nama_pasien=%s, isi_resep=%s, unggahan_resep=%s, tanggal_resep=%s, status_validasi=%s, no_telepon=%s, id_transaksi_pembelian=%s, email_apoteker=%s WHERE no_resep = %s",
						[nama_dokter,nama_pasien,isi_resep,unggahan_resep,tanggal_resep,status_validasi,no_telepon,id_transaksi_pembelian,email_apoteker, no_resep])
				return HttpResponseRedirect(reverse('transaksi_resep:transaksi_dengan_resep'))
			except Exception as e:
				return HttpResponseRedirect(reverse('transaksi_resep:transaksi_dengan_resep'))
	with connection.cursor() as c :
		c.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP WHERE no_resep = %s", [no_resep])
		res = dictfetchall(c)
	initial_data = {
		'no_resep' : res[0]['no_resep'],
		'nama_dokter' : res[0]['nama_dokter'],
		'nama_pasien' : res[0]['nama_pasien'],
		'isi_resep' : res[0]['isi_resep'],
		'unggahan_resep' : res[0]['unggahan_resep'],
		'tanggal_resep' : res[0]['tanggal_resep'],
		'status_validasi' : res[0]['status_validasi'],
		'no_telepon' : res[0]['no_telepon'],
		'id_transaksi_pembelian' : res[0]['id_transaksi_pembelian'],
		'email_apoteker' : res[0]['email_apoteker']
	}
	update_form_resep = updateForm(initial=initial_data)
	response = {'update_form_resep':update_form_resep }
	return render(request,'update_transaksi_resep.html',response)

def delete_transaksi_resep(request, no_resep):
	with connection.cursor() as c:
		c.execute("DELETE FROM TRANSAKSI_DENGAN_RESEP WHERE no_resep = %s", [no_resep])
	return HttpResponseRedirect(reverse('transaksi_resep:transaksi_dengan_resep'))

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]