from django.urls import path
from .views import *

app_name = 'profil'
urlpatterns = [
	path('', profil, name='profil')
]