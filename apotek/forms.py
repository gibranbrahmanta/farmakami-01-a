from django import forms

class createForm(forms.Form):

    nama_apotek = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Apotek',
        'type' : 'text',
        'required': True,
    }))

    alamat_apotek = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Alamat Apotek',
        'type' : 'text',
        'required': True,
    }))

    no_telp_apotek = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon Apotek',
        'type' : 'text',
        'required': False,
    }))

    nama_penyelenggara = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Nama Penyelenggara',
        'required': True,
    }))

    no_sia_penyelenggara = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Nomor SIA Penyelenggara Apotek',
        'required': True,
    }))

    email_penyelenggara = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Email Penyelenggara',
        'required': True,
    }))

class updateForm(forms.Form):

    id_apotek  = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'ID Apotek',
        'type' : 'text',
        'required': True,
        'readonly': True,
    }))
    
    nama_apotek = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Apotek',
        'type' : 'text',
        'required': True,
    }))

    alamat_apotek = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Alamat Apotek',
        'type' : 'text',
        'required': True,
    }))

    no_telp_apotek = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon Apotek',
        'type' : 'text',
        'required': False,
    }))

    nama_penyelenggara = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Nama Penyelenggara',
        'required': True,
    }))

    no_sia_penyelenggara = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Nomor SIA Penyelenggara Apotek',
        'required': True,
    }))

    email_penyelenggara = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Email Penyelenggara',
        'required': True,
    }))

