from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *


# Create your views here.
def rud_apotek(request):
    with connection.cursor() as c:
        c.execute("SELECT * FROM APOTEK")
        res = dictfetchall(c)
    response = {'data_apotek':res}
    return render(request,'rud_apotek.html',response)

def create_apotek(request):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('auth:login'))
    if request.session['role'] != 'admin_apotek':
        return HttpResponseRedirect(reverse('profil:profil'))
    if request.method == 'POST':
        form = createForm(request.POST)
        if form.is_valid():
            with connection.cursor() as c:
                c.execute("SELECT * FROM APOTEK ORDER BY id_apotek desc limit 1")
                last_data = dictfetchall(c)
            last_id = last_data[0]['id_apotek']
            updated_last_num = int(last_id[-1]) + 1
            id_apotek = 'APO' + str(updated_last_num)
            nama_apotek = form.cleaned_data['nama_apotek']
            alamat_apotek = form.cleaned_data['alamat_apotek']
            no_telp_apotek = form.cleaned_data['no_telp_apotek']
            nama_penyelenggara = form.cleaned_data['nama_penyelenggara']
            no_sia_penyelenggara = form.cleaned_data['no_sia_penyelenggara']
            email_penyelenggara = form.cleaned_data['email_penyelenggara']
            with connection.cursor() as c:
                c.execute("INSERT INTO APOTEK VALUES ( %s , %s , %s , %s, %s, %s, %s)",
                            [id_apotek, email_penyelenggara, no_sia_penyelenggara, nama_penyelenggara,
                            nama_apotek, alamat_apotek, no_telp_apotek]
                )
        return HttpResponseRedirect(reverse('apotek:rud_apotek'))
    create_form = createForm()
    response = {'create_form':create_form}
    return render(request,'create_apotek.html',response)

def update_apotek(request, key):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('auth:login'))
    if request.session['role'] != 'admin_apotek':
        return HttpResponseRedirect(reverse('apotek:rud_apotek'))
    if request.method == 'POST':
        form = updateForm(request.POST)
        if form.is_valid():
            id_apotek = form.cleaned_data['id_apotek']
            nama_apotek = form.cleaned_data['nama_apotek']
            alamat_apotek = form.cleaned_data['alamat_apotek']
            no_telp_apotek = form.cleaned_data['no_telp_apotek']
            nama_penyelenggara = form.cleaned_data['nama_penyelenggara']
            no_sia_penyelenggara = form.cleaned_data['no_sia_penyelenggara']
            email_penyelenggara = form.cleaned_data['email_penyelenggara']
            with connection.cursor() as c:
                c.execute("""INSERT INTO APOTEK VALUES ( %s , %s , %s , %s, %s, %s, %s) ON CONFLICT (id_apotek) 
                            DO UPDATE SET email = %s, no_sia =  %s, nama_penyelenggara =  %s, 
                            nama_apotek = %s, alamat_apotek = %s, telepon_apotek = %s""",
                            [id_apotek, email_penyelenggara, no_sia_penyelenggara, nama_penyelenggara,
                            nama_apotek, alamat_apotek, no_telp_apotek, email_penyelenggara, no_sia_penyelenggara, 
                            nama_penyelenggara, nama_apotek, alamat_apotek, no_telp_apotek]
                )
        return HttpResponseRedirect(reverse('apotek:rud_apotek'))
    with connection.cursor() as c:
        c.execute("SELECT * FROM APOTEK WHERE id_apotek = %s",[key])
        res = dictfetchall(c)
    initial_data = {
        'id_apotek' : res[0]['id_apotek'],
        'nama_apotek' : res[0]['nama_apotek'],
        'alamat_apotek' : res[0]['alamat_apotek'],
        'no_telp_apotek' : res[0]['telepon_apotek'],
        'nama_penyelenggara' : res[0]['nama_penyelenggara'],
        'no_sia_penyelenggara' : res[0]['no_sia'],
        'email_penyelenggara' : res[0]['email']
    }
    update_form = updateForm(initial=initial_data)
    response = {'update_form':update_form }
    return render(request,'update_apotek.html',response)

def delete_apotek(request,key):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('auth:login'))
    if request.session['role'] != 'admin_apotek':
        return HttpResponseRedirect(reverse('apotek:rud_apotek'))
    with connection.cursor() as c:
        c.execute("DELETE FROM APOTEK WHERE id_apotek = %s",[key])
    return HttpResponseRedirect(reverse('apotek:rud_apotek'))

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
