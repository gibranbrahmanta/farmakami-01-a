$(document).ready(function() {
    var form = $('.form-alamat');
    var add_button = $('.add-field');

    var pointer = 2;
    $(add_button).click(function(e) {
        $(form).append("<tr><td>+</td>" +
            "<td><input type='text' name='status-alamat-" + pointer + "'' class='form-control' placeholder='Status Alamat'></td>" +
            "<td>:</td>" +
            "<td><input type='text' name='alamat-" + pointer + "' class='form-control' placeholder='Alamat'></td></tr>"
        );
        pointer++;
        return false;
    });
});