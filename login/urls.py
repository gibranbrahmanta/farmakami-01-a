from django.urls import path
from .views import *

app_name = 'auth'
urlpatterns = [
	path('register_admin', register_admin, name='register_admin'),
	path('register_konsumen',register_konsumen, name='register_konsumen'),
    path('register_kurir',register_kurir, name='register_kurir'),
    path('register_cs',register_CS, name='register_cs'),
    path('login',login, name='login'),
    path('logout',logout, name='logout'),
]