from django import forms


class createForm(forms.Form):

    nama_alat_medis = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Alat Medis',
        'type' : 'text',
        'required': True,
    }))

    deskripsi_alat = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Deskripsi',
        'type' : 'text',
    }), required = False)

    jenis_penggunaan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jenis Penggunaan',
        'type' : 'text',
        'required': True,
    }))


class updateForm(forms.Form):

    id_alat_medis = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'required': True,
        'readonly' : True,
    }))

    nama_alat_medis = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Alat Medis',
        'type' : 'text',
        'required': True,
    }))

    deskripsi_alat = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Deskripsi',
        'type' : 'text',
    }), required = False)

    jenis_penggunaan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jenis Penggunaan',
        'type' : 'text',
        'required': True,
    }))

    id_produk = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Unggahan Resep',
        'required': True,
        'readonly' : True,
    }))
