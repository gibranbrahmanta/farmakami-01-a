from django import forms
from django.db import connection

search_path = "SET search_path to FARMAKAMI"

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def get_id_produk():
    with connection.cursor() as c:
        c.execute(search_path)
        c.execute("SELECT DISTINCT id_produk FROM PRODUK_APOTEK ORDER BY id_produk ASC")
        res = dictfetchall(c)
    temp_res = [res[i]['id_produk'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]

def get_id_apotek():
    with connection.cursor() as ci:
        ci.execute(search_path)
        ci.execute("SELECT DISTINCT id_apotek FROM PRODUK_APOTEK ORDER BY id_apotek ASC")
        res = dictfetchall(ci)
    temp_res = [res[i]['id_apotek'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]

def get_id_transaksi():
    with connection.cursor() as cii:
        cii.execute(search_path)
        cii.execute("SELECT DISTINCT id_transaksi_pembelian FROM transaksi_pembelian ORDER BY id_transaksi_pembelian ASC")
        res = dictfetchall(cii)
    temp_res = [res[i]['id_transaksi_pembelian'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]



class createForm(forms.Form):

    id_produk = forms.CharField(label="ID Produk", widget=forms.Select(choices=get_id_produk()))

    id_apotek = forms.CharField(label="ID Apotek", widget=forms.Select(choices=get_id_apotek()))

    id_transaksi_pembelian = forms.CharField(label="ID Transaksi", widget=forms.Select(choices=get_id_transaksi())) 
    
    jumlah = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jumlah',
        'type' : 'text',
        'required': True,
    }))


class updateForm(forms.Form):

    id_produk  = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'ID Produk',
        'type' : 'text',
        'required': True,
        'readonly': True,
    }))

    id_apotek  = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'ID Apotek',
        'type' : 'text',
        'required': True,
        'readonly': True,
    }))

    id_transaksi_pembelian = forms.CharField(label="ID Transaksi Pembelian", widget=forms.Select(choices=get_id_transaksi()))

    jumlah = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jumlah',
        'type' : 'integer',
        'required': True,
    }))