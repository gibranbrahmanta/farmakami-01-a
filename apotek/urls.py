from django.urls import path
from .views import *

app_name = 'apotek'
urlpatterns = [
	path('', rud_apotek, name='rud_apotek'),
	path('create_apotek', create_apotek, name='create_apotek'),
	path('update_apotek/<str:key>',update_apotek, name='update_apotek'),
	path('delete_apotek/<str:key>',delete_apotek, name='delete_apotek')
]