from django import forms
from django.db import connection


lst_status = ['Entered','Processing','Done']
tuple_status =  [tuple([x,x]) for x in lst_status]

search_path = "SET search_path to FARMAKAMI"

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def get_id_transaksi():
    with connection.cursor() as c:
        c.execute(search_path)
        c.execute("SELECT id_transaksi_pembelian FROM transaksi_pembelian ORDER BY id_transaksi_pembelian ASC")
        res = dictfetchall(c)
    temp_res = [res[i]['id_transaksi_pembelian'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]

def get_email_apoteker():
    with connection.cursor() as c:
        c.execute(search_path)
        c.execute("SELECT email FROM apoteker ORDER BY email ASC")
        res = dictfetchall(c)
    temp_res = [res[i]['email'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]

class createForm(forms.Form):

    nama_dokter = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Dokter',
        'type' : 'text',
        'required': True,
    }))

    nama_pasien = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Pasien',
        'type' : 'text',
        'required': True,
    }))

    isi_resep = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Isi Resep',
        'type' : 'text',
        'required': True,
    }))

    unggahan_resep = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Unggahan Resep',
    }), required = False)

    tanggal_resep = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'YYYY-MM-DD',
        'required': True,
    }))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon Pasien',
        'type' : 'text',
    }), required = False)

    id_transaksi_pembelian = forms.CharField(label="ID Transaksi", widget=forms.Select(choices=get_id_transaksi())) 


class updateForm(forms.Form):

    no_resep = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'required': True,
        'readonly' : True,
    }))

    nama_dokter = forms.CharField( widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Dokter',
        'type' : 'text',
        'required': True,
    }))

    nama_pasien = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Pasien',
        'type' : 'text',
        'required': True,
    }))

    isi_resep = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Isi Resep',
        'type' : 'text',
        'required': True,
    }))

    unggahan_resep = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Unggahan Resep',
    }), required = False)

    tanggal_resep = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'YYYY-MM-DD',
        'required': True,
    }))

    status_validasi = forms.CharField(label="Status", widget=forms.Select(choices=tuple_status))

    no_telepon = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Telepon Pasien',
        'type' : 'text',
    }), required = False)

    id_transaksi_pembelian = forms.CharField(label="ID Transaksi", widget=forms.Select(choices=get_id_transaksi())) 

    email_apoteker = forms.CharField(label="Email Apoteker", widget=forms.Select(choices=get_email_apoteker()))


