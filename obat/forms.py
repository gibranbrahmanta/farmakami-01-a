from django import forms
from django.db import connection


c = connection.cursor()
c.execute("SELECT id_merk_obat FROM FARMAKAMI.MERK_OBAT")
select_merkob = c.fetchall()

opt_id_merk_obat = [tuple([select_merkob[x][0], select_merkob[x][0]]) for x in range(len(select_merkob))]

class createForm(forms.Form):

    id_merk_obat = forms.CharField(widget=forms.Select(choices=opt_id_merk_obat, attrs={
        'class': 'form-control',
        'placeholder': 'ID Merk Obat', 
        'required': True,
    }))

    netto = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Netto',
        'type' : 'text',
        'required': True,
    }))

    dosis = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Dosis',
        'type' : 'text',
        'required': True,
    }))

    aturan_pakai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Aturan Pakai',
        'type' : 'text',
        'required': False,
    }))

    kontraindaksi = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kontraindaksi',
        'type' : 'text',
        'required': False,
    }))

    bentuk_kesediaan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Bentuk Kesediaan',
        'type' : 'text',
        'required': True,
    }))

class updateForm(forms.Form):

    id_obat = forms.CharField(widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'ID Obat',
            'type' : 'text',
            'required': True,
            'readonly' : True,
    }))

    id_produk = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'ID Produk',
        'type' : 'text',
        'required': True,
        'readonly' : True,
    }))

    id_merk_obat = forms.CharField(widget=forms.Select(choices=opt_id_merk_obat, attrs={
        'class': 'form-control',
        'placeholder': 'ID Merk Obat', 
        'required': True,
    }))

    netto = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Netto',
        'type' : 'text',
        'required': True,
    }))

    dosis = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Dosis',
        'type' : 'text',
        'required': True,
    }))

    aturan_pakai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Aturan Pakai',
        'type' : 'text',
        'required': False,
    }))

    kontraindaksi = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kontraindaksi',
        'type' : 'text',
        'required': False,
    }))

    bentuk_kesediaan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Bentuk Kesediaan',
        'type' : 'text',
        'required': True,
    }))
