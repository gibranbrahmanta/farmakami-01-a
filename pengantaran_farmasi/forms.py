from django import forms
from django.db import connection
import datetime

lst = ['PGN'+str(i) for i in range(1,11)]
lst1 = ['KR'+str(i) for i in range(1,8)]
lst2 = ['TP'+str(i) for i in range(1,16)]
lst3 = ['TP000', 'TP001','TP005','TP006','TP007','TP008','TP009','TP010','TP011','TP012']
INTEGER_CHOICES= [tuple([x,x]) for x in lst]
INTEGER_CHOICES2= [tuple([x,x]) for x in lst1]
INTEGER_CHOICES3= [tuple([x,x]) for x in lst2]
INTEGER_CHOICES4= [tuple([x,x]) for x in lst3]

search_path = "SET search_path to FARMAKAMI"

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
def get_id_transaksi():
    with connection.cursor() as cii:
        cii.execute(search_path)
        cii.execute("SELECT DISTINCT id_transaksi_pembelian FROM transaksi_pembelian ORDER BY id_transaksi_pembelian ASC")
        res = dictfetchall(cii)
    temp_res = [res[i]['id_transaksi_pembelian'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]

def get_id_kurir():
    with connection.cursor() as cii:
        cii.execute(search_path)
        cii.execute("SELECT DISTINCT id_kurir FROM KURIR")
        res = dictfetchall(cii)
    temp_res = [res[i]['id_kurir'] for i in range(len(res))]
    return [tuple([i,i]) for i in temp_res]



class createForm(forms.Form):

    id_transaksi_pembelian = forms.CharField(label="ID Transaksi", widget=forms.Select(choices=get_id_transaksi()))
    waktu = forms.DateField(initial=datetime.date.today, label='Waktu')
    biaya_kirim = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Biaya Kirim',
        'type' : 'integer',
        'required': True,
    }))

class updateForm(forms.Form):
    id_pengantaran  = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'ID Produk',
        'type' : 'text',
        'required': True,
        'readonly': True,
    }))

    id_kurir = forms.CharField(label="ID Kurir", widget=forms.Select(choices=get_id_kurir()))

    id_transaksi_pembelian = forms.CharField(label="ID Transaksi", widget=forms.Select(choices=get_id_transaksi()))

    waktu = forms.DateField(initial=datetime.date.today, label='Waktu')

    status_pengantaran = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Status',
        'type' : 'text',
        'required': True,
    }))

    biaya_kirim = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Biaya Kirim',
        'type' : 'integer',
        'required': True,
    }))

    total_biaya = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Total Biaya',
        'type' : 'integer',
        'required': True,
        'readonly': True,
    }))



