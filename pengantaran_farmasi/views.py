from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from collections import namedtuple
import datetime

from .forms import *

# Create your views here.
def rud_pengantaran_farmasi(request):
    with connection.cursor() as c:
        c.execute("SELECT * FROM PENGANTARAN_FARMASI")
        res = dictfetchall(c)
        response = {'data_pengantaran_farmasi':res}
    return render(request,'rud_pengantaran_farmasi.html',response)

# def update_pengantaran_farmasi(request):
#     update_form = updateForm()
#     response = {'update_form':update_form }
#     return render(request,'update_pengantaran_farmasi.html',response)

# def create_pengantaran_farmasi(request):
#     create_form = createForm()
#     response = {'create_form':create_form }
#     return render(request,'create_pengantaran_farmasi.html',response)
# def create_pengantaran_farmasi(request):
#     if request.method == 'POST':
#         form = createForm(request.POST)
#         if form.is_valid():
#             with connection.cursor() as ci:
#                 ci.execute("SELECT * FROM PENGANTARAN FARMASI ORDER BY id_pengantaran desc limit 1")
#                 last_data = dictfetchall(ci)
#             last_id = last_data[0]['id_pengantaran']
# 			updated_id = int(last_id[3:]) + 1
# 			updated_id_str = '{:03d}'.format(updated_id)
# 			id_pengantaran = 'PGN' + updated_id_str
#             id_kurir = 'KR001'
# 			waktu = form.cleaned_data['waktu']
# 			biaya_kirim = form.cleaned_data['biaya_kirim']
#             status_pengantaran = "Di Gudang"
#             total_biaya = form.cleaned_data['total_biaya']
#             with connection.cursor() as c:
# 				c.execute("INSERT INTO PENGANTARAN_FARMASI VALUES ( %s, %s, %s, %s, %s, %s)", 
# 					[id_pengantaran, id_kurir, waktu, status_pengantaran, biaya_kirim, total_biaya])
#         return HttpResponseRedirect(reverse('pengantaran_farmasi:rud_pengantaran_farmasi'))
# 	create_form = createForm()
# 	response = {'create_form' : create_form}
# 	return render(request, 'create_pengantaran_farmasi.html', response)

        

def create_pengantaran_farmasi(request):
    if request.method == 'POST':
        form = createForm(request.POST)
        if form.is_valid():
            with connection.cursor() as c:
                c.execute("SELECT * FROM PENGANTARAN_FARMASI ORDER BY id_pengantaran desc limit 1")
                last_data = dictfetchall(c)
            last_id = last_data[0]['id_pengantaran']
            updated_id = int(last_id[3:]) + 1
            updated_id_str = '{:03d}'.format(updated_id)
            id_pengantaran = 'PGN' + updated_id_str
            id_kurir = 'KR001'
            id_transaksi_pembelian = form.cleaned_data['id_transaksi_pembelian']
            waktu = form.cleaned_data['waktu']
            biaya_kirim = form.cleaned_data['biaya_kirim']
            status_pengantaran = 'Belum Diproses'
            total_biaya = 0
            with connection.cursor() as cii:
                cii.execute("INSERT INTO PENGANTARAN_FARMASI VALUES ( %s, %s, %s, %s, %s, %s, %s)",[id_pengantaran, id_kurir, id_transaksi_pembelian, waktu, status_pengantaran, biaya_kirim, total_biaya])
        return HttpResponseRedirect(reverse('pengantaran_farmasi:rud_pengantaran_farmasi'))
    create_form = createForm()
    response = {'create_form' : create_form}
    return render(request, 'create_pengantaran_farmasi.html', response)

def update_pengantaran_farmasi(request, id_pengantaran):
    if request.method == 'POST':
        form = updateForm(request.POST)
        if form.is_valid():
            id_pengantaran = form.cleaned_data['id_pengantaran']
            id_kurir = form.cleaned_data['id_kurir']
            id_transaksi_pembelian = form.cleaned_data['id_transaksi_pembelian']
            waktu = form.cleaned_data['waktu']
            status_pengantaran = form.cleaned_data['status_pengantaran']
            biaya_kirim = form.cleaned_data['biaya_kirim']
            total_biaya = form.cleaned_data['total_biaya']
            with connection.cursor() as c:
                c.execute("""INSERT INTO PENGANTARAN_FARMASI VALUES ( %s , %s , %s , %s, %s, %s, %s) ON CONFLICT (id_pengantaran) 
                            DO UPDATE SET total_biaya = %s, biaya_kirim =  %s, status_pengantaran =  %s, 
                            id_kurir = %s, id_transaksi_pembelian = %s, waktu = %s""",
                            [id_pengantaran, id_kurir, id_transaksi_pembelian, waktu, status_pengantaran, biaya_kirim, total_biaya, total_biaya, biaya_kirim, status_pengantaran, id_kurir, id_transaksi_pembelian, waktu]
                )
        return HttpResponseRedirect(reverse('pengantaran_farmasi:rud_pengantaran_farmasi'))
    with connection.cursor() as c:
        c.execute("SELECT * FROM PENGANTARAN_FARMASI WHERE id_pengantaran = %s", [id_pengantaran])
        res = dictfetchall(c)
    initial_data = {
        'id_pengantaran' : res[0]['id_pengantaran'],
        'id_kurir' : res[0]['id_kurir'],
        'id_transaksi_pembelian' : res[0]['id_transaksi_pembelian'],
        'waktu' : res[0]['waktu'],
        'status_pengantaran' : res[0]['status_pengantaran'],
        'biaya_kirim' : res[0]['biaya_kirim'],
        'total_biaya' : res[0]['total_biaya']
    }
    update_form_pengantaran = updateForm(initial = initial_data)
    response = {'update_form_pengantaran':update_form_pengantaran}
    return render(request,'update_pengantaran_farmasi.html',response)

def delete_pengantaran_farmasi(request,id_pengantaran):
    with connection.cursor() as c:
        c.execute("DELETE FROM PENGANTARAN_FARMASI WHERE id_pengantaran = %s",[id_pengantaran])
    return HttpResponseRedirect(reverse('pengantaran_farmasi:rud_pengantaran_farmasi'))

def dictfetchall(cursor):
	"Return all rows from a cursor as a dict"
	columns = [col[0] for col in cursor.description]
	return [
		dict(zip(columns, row))
		for row in cursor.fetchall()
	]
