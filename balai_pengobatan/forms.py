from django import forms
from django.db import connection

cursor = connection.cursor()
cursor.execute("SELECT id_apotek FROM FARMAKAMI.APOTEK")
select = cursor.fetchall()

id_apotek_berasosiasi = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]


class createForm(forms.Form):

    alamat_balai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Alamat Balai',
        'type' : 'text',
        'required': True,
    }))

    nama_balai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Balai',
        'type' : 'text',
        'required': True,
    }))

    jenis_balai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jenis Balai',
        'type' : 'text',
        'required': True,
    }))

    telepon_balai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Telepon Balai',
        'required': False,
    }))

    id_apotek = forms.CharField(widget=forms.Select(choices=id_apotek_berasosiasi, attrs={
        'class': 'form-control',
        'placeholder' : 'Apotek Bersosiasi',
        'required': True,
    }))


class updateForm(forms.Form):

    id_balai  = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'ID Balai',
        'type' : 'text',
        'required': True,
        'readonly': True,
    }))
    
    alamat_balai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Alamat Balai',
        'type' : 'text',
        'required': True,
    }))

    nama_balai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Balai',
        'type' : 'text',
        'required': True,
    }))

    jenis_balai  = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jenis Balai',
        'type' : 'text',
        'required': True,
    }))

    telepon_balai = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Nomor Telepon Balai',
        'required': False,
    }))

    id_apotek = forms.CharField(widget=forms.Select(choices=id_apotek_berasosiasi, attrs={
        'class': 'form-control',
        'required': True,
    }))

